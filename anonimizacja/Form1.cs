﻿using anonimizacja.Authentication;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace anonimizacja
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            
            InitializeComponent();
            this.panel1.BackColor = Color.FromArgb(234, 76, 37);
            this.ShowInTaskbar = true;
            this.ControlBox = true;
            
        }

        private void textbox_username_TextChanged(object sender, EventArgs e)
        {

        }

        private void button_login_Click(object sender, EventArgs e)
        {
            this.Hide();
            var MainApp = new MainForm();
            MainApp.Show();
           
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //this.BackColor = Color.FromArgb(219, 53, 28);
            this.button_login.ForeColor = Color.FromArgb(34, 36, 49);
            this.button_login.BackColor = Color.FromArgb(255, 255, 255);
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
